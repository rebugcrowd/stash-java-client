package com.atlassian.stash.rest.client.bamboo.specs;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.stash.rest.client.bamboo.specs.StashJavaClientBambooSpec.IntegrationTestTargetProduct.BITBUCKET_4_13_1;
import static com.atlassian.stash.rest.client.bamboo.specs.StashJavaClientBambooSpec.IntegrationTestTargetProduct.BITBUCKET_5_14_0;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class StashJavaClientBambooSpecTest {
    private StashJavaClientBambooSpec spec;

    @Before
    public void setUp() throws Exception {
        spec = new StashJavaClientBambooSpec();
    }

    @Test
    public void checkYourPlanOffline() throws PropertiesValidationException {
        final Plan plan = spec.buildPlan();

        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void testIntegrationTestsProductName() throws Exception {
        assertThat(BITBUCKET_4_13_1.getProductName(), equalTo("bitbucket"));
    }

    @Test
    public void testBuildIntegrationTestJob() throws Exception {
        final Job job = spec.buildIntegrationTestJob(BITBUCKET_5_14_0);
        final JobProperties jobProps = EntityPropertiesBuilders.build(job);

        assertThat("job key", jobProps.getKey().getKey(), equalTo("ITBITBUCKET5140"));
        assertThat("job name", jobProps.getName(), equalTo("it-bitbucket-5.14.0"));
    }
}
