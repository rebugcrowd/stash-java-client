package com.atlassian.stash.rest.client.api.entity;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

/**
 * A report for code insights.
 */
public class Report {
    /**
     * The result of a report. A single report may be consider as successful ({@link Result#PASS}) or failed
     * ({@link Result#FAIL}).
     */
    public enum Result {
        PASS, FAIL
    }

    @Nonnull
    private final String key;
    @Nonnull
    private final String title;
    @Nonnull
    private final List<ReportDataEntry> data;
    @Nullable
    private final String details;
    @Nullable
    private final String vendor;
    @Nullable
    public final String link;
    @Nullable
    private final String logoUrl;
    @Nullable
    private final Result result;

    @Nullable
    public Report(@Nonnull String key,
                  @Nonnull String title,
                  @Nullable Iterable<ReportDataEntry> data,
                  @Nullable String details,
                  @Nullable String vendor,
                  @Nullable String link,
                  @Nullable String logoUrl,
                  @Nullable Result result) {
        this.key = key;
        this.title = title;
        this.data = data != null ? ImmutableList.copyOf(data) : Collections.emptyList();
        this.details = details;
        this.vendor = vendor;
        this.link = link;
        this.logoUrl = logoUrl;
        this.result = result;
    }

    /**
     * Unique key of the report.
     */
    @Nonnull
    public String getKey() {
        return key;
    }

    /**
     * Title of the report.
     */
    @Nonnull
    public String getTitle() {
        return title;
    }

    /**
     * Custom data defined by this report.
     */
    @Nonnull
    public List<ReportDataEntry> getData() {
        return data;
    }

    /**
     * Detail message of the report.
     */
    @Nullable
    public String getDetails() {
        return details;
    }

    /**
     * Vendor/tool which produced this report.
     */
    @Nullable
    public String getVendor() {
        return vendor;
    }

    /**
     * URL for the vendor/tool which produced the report.
     */
    @Nullable
    public String getLink() {
        return link;
    }

    /**
     * URL of the logo for the vendor/tool which produced the report.
     */
    @Nullable
    public String getLogoUrl() {
        return logoUrl;
    }

    /**
     * Result of the report - whether it failed or passed.
     */
    @Nullable
    public Result getResult() {
        return result;
    }
}
