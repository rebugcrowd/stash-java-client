package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.MoreObjects;

/**
 * Generic representation of SSH public key
 */
public class SshKey {
    protected final long id;
    protected final String text;
    protected final String label;

    public SshKey(final String text, final String label, final long id) {
        this.text = text;
        this.label = label;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    /**
     * @return the public key value in PKCS format
     */
    public String getText() {
        return text;
    }

    /**
     * @return the public key label
     */
    public String getLabel() {
        return label;
    }

    protected MoreObjects.ToStringHelper toStringHelper() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("text", text)
                .add("label", label);
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }
}
