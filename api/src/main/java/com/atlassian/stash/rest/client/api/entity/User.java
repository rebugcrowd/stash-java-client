package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * User class attributes based on https://developer.atlassian.com/static/javadoc/bitbucket-server/4.0.0/api/reference/com/atlassian/bitbucket/user/DetailedUser.html
 *
 */
public class User {

    private final String name;

    private final String emailAddress;

    private final long id;

    private final String displayName;

    private final boolean active;

    private final String slug;

    private final String type;

    private final String directoryName;

    private final boolean deletable;

    private final Date lastAuthenticationTimestamp;

    private final boolean mutableDetails;

    private final boolean mutableGroups;

    private final String selfUrl;

    public User(@Nonnull final String name, @Nullable final String emailAddress, final long id, @Nonnull final String displayName,
                final boolean active, @Nonnull final String slug, @Nonnull final String type,
                @Nullable final String directoryName, final boolean deletable, @Nullable final Date lastAuthenticationTimestamp,
                final boolean mutableDetails, final boolean mutableGroups, @Nullable final String selfUrl) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.id = id;
        this.displayName = displayName;
        this.active = active;
        this.slug = slug;
        this.type = type;
        this.directoryName = directoryName;
        this.deletable = deletable;
        this.lastAuthenticationTimestamp = lastAuthenticationTimestamp;
        this.mutableDetails = mutableDetails;
        this.mutableGroups = mutableGroups;
        this.selfUrl = selfUrl;
    }

    public String getName() {
        return name;
    }

    @Nullable
    public String getEmailAddress() {
         return emailAddress;
    }

    public long getId() {
         return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isActive() {
        return active;
    }

    public String getSlug() {
        return slug;
    }

    public String getType() {
        return type;
    }

    @Nullable
    public String getDirectoryName() {
        return directoryName;
    }

    public boolean isDeletable() {
        return deletable;
    }

    @Nullable
    public Date getLastAuthenticationTimestamp() {
        return lastAuthenticationTimestamp;
    }

    public boolean isMutableDetails() {
        return mutableDetails;
    }

    public boolean isMutableGroups() {
        return mutableGroups;
    }

    public String getSelfUrl() {
        return selfUrl;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                      .add("name", name)
                      .add("emailAddress", emailAddress)
                      .add("id", id)
                      .add("displayName", displayName)
                      .add("isActive", active)
                      .add("slug", slug)
                      .add("type", type)
                      .add("directoryName", directoryName)
                      .add("isDeletable", deletable)
                      .add("lastAuthenticationTimestamp", lastAuthenticationTimestamp)
                      .add("isMutableDetails", mutableDetails)
                      .add("isMutableGroups", mutableGroups)
                      .add("selfUrl", selfUrl)
                      .toString();
    }


}
