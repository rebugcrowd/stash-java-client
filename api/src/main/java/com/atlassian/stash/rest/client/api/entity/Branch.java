package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.MoreObjects;

/**
 * Describes a repository branch
 *
 * @see <a href="https://developer.atlassian.com/static/rest/stash/2.11.3/stash-rest.html#idp3259072">Stash documentation: branches</a>
 */
public class Branch {
    private final String id;
    private final String displayId;
    private final String latestChangeset;
    private final boolean isDefault;

    public Branch(final String id, final String displayId, final String latestChangeset, final boolean isDefault) {
        this.id = id;
        this.displayId = displayId;
        this.latestChangeset = latestChangeset;
        this.isDefault = isDefault;
    }

    /**
     * @return fully qualified branch identifier eg. refs/heads/master
     */
    public String getId() {
        return id;
    }

    /**
     * @return branch display identifier eg. master
     */
    public String getDisplayId() {
        return displayId;
    }

    /**
     * @return branch head changeset
     */
    public String getLatestChangeset() {
        return latestChangeset;
    }

    /**
     * @return If true branch is shown as default branch in Stash UI, false otherwise.
     */
    public boolean isDefault() {
        return isDefault;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("displayId", displayId)
                .add("latestChangeset", latestChangeset)
                .add("default", isDefault)
                .toString();
    }
}
