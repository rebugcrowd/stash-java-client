package com.atlassian.stash.rest.client.api.entity;

/**
 * Project-level permissions for users and groups
 * <p>
 * See https://confluence.atlassian.com/display/BitbucketServer/Using+project+permissions
 */
public enum ProjectPermission {
  /**
   * Allows read access to a project.
   * <p>
   * Browse, clone/pull and create/browse/comment on pull requests for the project
   * repos.
   */
  PROJECT_READ,

  /**
   * Allows write access to a project.
   * <p>
   * In addition to all {@link #PROJECT_READ} permissions, merging pull requests and
   * pushing to project repos is allowed.
   */
  PROJECT_WRITE,

  /**
   * Administration level project access.
   * <p>
   * In addition to all {@link #PROJECT_WRITE} permissions, new repos may be created.
   * Moreover, the project settings may be changed.
   */
  PROJECT_ADMIN,
}
