package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.StashError;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestParticipant;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Report;
import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.gson.JsonElement;

import java.util.List;
import java.util.function.Function;

public class Parsers {
    public static <T> Function<JsonElement, T> propertyParser(String property, Function<JsonElement, T> parser) {
        return new PropertyParser<>(property, parser);
    }

    public static <T> Function<JsonElement, List<T>> listParser(Function<JsonElement, T> elementParser) {
        return new ListParser<>(elementParser);
    }

    public static Function<JsonElement, Branch> branchParser() {
        return BRANCH_PARSER;
    }

    public static Function<JsonElement, Tag> tagParser() {
        return TAG_PARSER;
    }

    public static Function<JsonElement, Link> linkParser(String hrefProperty, String nameProperty) {
        return new LinkParser(hrefProperty, nameProperty);
    }

    public static <T> Function<JsonElement, Page<T>> pageParser(Function<JsonElement, T> valueParser) {
        return new PageParser<>(valueParser);
    }

    public static Function<JsonElement, Project> projectParser() {
        return PROJECT_PARSER;
    }

    public static Function<JsonElement, ProjectGroupPermission> projectGroupPermissionParser() {
        return PROJECT_GROUP_PERMISSION_PARSER;
    }

    public static Function<JsonElement, ProjectUserPermission> projectUserPermissionParser() {
        return PROJECT_USER_PERMISSION_PARSER;
    }

    public static Function<JsonElement, PullRequestParticipant> pullRequestParticipantParser() {
        return PULL_REQUEST_PARTICIPANT_PARSER;
    }

    public static Function<JsonElement, PullRequestRef> pullRequestRefParser() {
        return PULL_REQUEST_REF_PARSER;
    }

    public static Function<JsonElement, PullRequestStatus> pullRequestStatusParser() {
        return PULL_REQUEST_STATUS_PARSER;
    }

    public static Function<JsonElement, PullRequestMergeability> pullRequestMergeabilityParser() {
        return PULL_REQUEST_MERGEABILITY_PARSER;
    }

    public static Function<JsonElement, Repository> repositoryParser() {
        return REPOSITORY_PARSER;
    }

    public static Function<JsonElement, RepositorySshKey> repositorySshKeyParser() {
        return REPOSITORY_SSH_KEY_PARSER;
    }

    public static Function<JsonElement, UserSshKey> userSshKeyParser() {
        return USER_SSH_KEY_PARSER;
    }

    public static Function<JsonElement, Comment> commentParser() {
        return COMMENT_PARSER;
    }

    public static Function<JsonElement, Task> taskParser() {
        return TASK_PARSER;
    }

    public static Function<JsonElement, ApplicationProperties> applicationPropertiesParser() {
        return APPLICATION_PROPERTIES_PARSER;
    }

    public static Function<JsonElement, StashError> errorParser() {
        return ERROR_PARSER;
    }

    public static Function<JsonElement, List<StashError>> errorsParser() {
        return ERRORS_PARSER;
    }

    public static Function<JsonElement, MirrorServer> mirrorParser() {
        return MIRROR_SERVER_PARSER;
    }

    public static Function<JsonElement, User> userParser() {
        return USER_PARSER;
    }

    public static Function<JsonElement, Report> reportParser() {
        return REPORT_PARSER;
    }

    public static Function<JsonElement, ReportDataEntry> reportDataEntryParser() {
        return REPORT_DATA_ENTRY_PARSER;
    }

    public static Function<JsonElement, CodeAnnotation> codeAnnotationParser() {
        return CODE_ANNOTATION_PARSER;
    }

    private static final TagParser TAG_PARSER = new TagParser();
    private static final BranchParser BRANCH_PARSER = new BranchParser();
    private static final ProjectParser PROJECT_PARSER = new ProjectParser();
    private static final ProjectGroupPermissionParser PROJECT_GROUP_PERMISSION_PARSER = new ProjectGroupPermissionParser();
    private static final ProjectUserPermissionParser PROJECT_USER_PERMISSION_PARSER = new ProjectUserPermissionParser();
    private static final RepositoryParser REPOSITORY_PARSER = new RepositoryParser();
    private static final RepositorySshKeyParser REPOSITORY_SSH_KEY_PARSER = new RepositorySshKeyParser();
    private static final UserSshKeyParser USER_SSH_KEY_PARSER = new UserSshKeyParser();
    private static final ErrorParser ERROR_PARSER = new ErrorParser();
    private static final ErrorsParser ERRORS_PARSER = new ErrorsParser();
    private static final PullRequestStatusParser PULL_REQUEST_STATUS_PARSER = new PullRequestStatusParser();
    private static final PullRequestParticipantParser PULL_REQUEST_PARTICIPANT_PARSER = new PullRequestParticipantParser();
    private static final PullRequestRefParser PULL_REQUEST_REF_PARSER = new PullRequestRefParser();
    private static final PullRequestMergeabilityParser PULL_REQUEST_MERGEABILITY_PARSER = new PullRequestMergeabilityParser();
    private static final CommentParser COMMENT_PARSER = new CommentParser();
    private static final TaskParser TASK_PARSER = new TaskParser();
    private static final ApplicationPropertiesParser APPLICATION_PROPERTIES_PARSER = new ApplicationPropertiesParser();
    private static final MirrorServerParser MIRROR_SERVER_PARSER = new MirrorServerParser();
    private static final UserParser USER_PARSER = new UserParser();
    private static final ReportParser REPORT_PARSER = new ReportParser();
    private static final ReportDataEntryParser REPORT_DATA_ENTRY_PARSER = new ReportDataEntryParser();
    private static final CodeAnnotationParser CODE_ANNOTATION_PARSER = new CodeAnnotationParser();
}
