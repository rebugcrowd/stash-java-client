package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;

/**
 * Request object for sending more than one {@link CodeAnnotation} entities via REST.
 *
 * @see CodeAnnotationsRequest#toJson()
 */
public class CodeAnnotationsRequest {
    @Nonnull
    private final Iterable<CodeAnnotation> annotations;

    public CodeAnnotationsRequest(@Nonnull Iterable<CodeAnnotation> codeAnnotations) {
        this.annotations = ImmutableList.copyOf(codeAnnotations);
    }

    public JsonObject toJson() {
        final JsonArray jsonArray = new JsonArray();
        for (CodeAnnotation annotation : annotations) {
            jsonArray.add(new CodeAnnotationRequest(annotation).toJson());
        }

        final JsonObject jsonObject = new JsonObject();
        jsonObject.add("annotations", jsonArray);
        return jsonObject;
    }
}
