package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.api.AvatarRequest;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.StashError;
import com.atlassian.stash.rest.client.api.StashException;
import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Report;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.atlassian.stash.rest.client.api.entity.TaskState;
import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.entity.CodeAnnotationRequest;
import com.atlassian.stash.rest.client.core.entity.CodeAnnotationsRequest;
import com.atlassian.stash.rest.client.core.entity.CreateCommentRequest;
import com.atlassian.stash.rest.client.core.entity.CreateTaskRequest;
import com.atlassian.stash.rest.client.core.entity.ReportRequest;
import com.atlassian.stash.rest.client.core.entity.StashCreateProjectKeyRequest;
import com.atlassian.stash.rest.client.core.entity.StashCreatePullReqRequest;
import com.atlassian.stash.rest.client.core.entity.StashCreateRepositoryRequest;
import com.atlassian.stash.rest.client.core.entity.StashForkRepositoryRequest;
import com.atlassian.stash.rest.client.core.entity.StashRepositorySshKeyRequest;
import com.atlassian.stash.rest.client.core.entity.StashUpdateProjectRequest;
import com.atlassian.stash.rest.client.core.entity.StashUserSshKeyRequest;
import com.atlassian.stash.rest.client.core.entity.UpdateTaskRequest;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpMethod;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponse;
import com.atlassian.stash.rest.client.core.http.UriBuilder;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import static com.atlassian.stash.rest.client.api.StashException.toErrors;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.DELETE;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.GET;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.POST;
import static com.atlassian.stash.rest.client.core.http.HttpMethod.PUT;
import static com.atlassian.stash.rest.client.core.parser.Parsers.applicationPropertiesParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.branchParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.codeAnnotationParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.commentParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.errorsParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.mirrorParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pageParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.projectGroupPermissionParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.projectParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.projectUserPermissionParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.propertyParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pullRequestMergeabilityParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pullRequestStatusParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.reportParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.repositoryParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.repositorySshKeyParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.tagParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.taskParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.userParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.userSshKeyParser;
import static java.net.HttpURLConnection.HTTP_ENTITY_TOO_LARGE;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class StashClientImpl implements StashClient {
    private static final Logger log = Logger.getLogger(StashClientImpl.class);
    private final HttpExecutor httpExecutor;
    private final int pageLimit;

    private static final BiConsumer<UriBuilder, Long> START_PARAM_ENCODER =
            (uriBuilder, start) -> uriBuilder.addQueryParam("start", Long.toString(start));
    private static final BiConsumer<UriBuilder, Long> LIMIT_PARAM_ENCODER =
            (uriBuilder, limit) -> uriBuilder.addQueryParam("limit", (limit > 0) ? Long.toString(limit) : null);
    private static final BiConsumer<UriBuilder, AvatarRequest> AVATAR_REQUEST_ENCODER =
            (uriBuilder, avatarRequest) -> {
                if (null != avatarRequest) {
                    uriBuilder.addQueryParam("avatarSize", (avatarRequest.getSize() > 0) ?
                            Long.toString(avatarRequest.getSize()) : null);
                    uriBuilder.addQueryParam("avatarScheme", avatarRequest.getUrlScheme());
                    uriBuilder.addQueryParam("avatarUrlMode", avatarRequest.getUrlMode());
                }
            };
    private static final BiConsumer<UriBuilder, PullRequestDirection> DIRECTION_ENCODER =
            ((uriBuilder, direction) -> uriBuilder.addQueryParam("direction", direction.name()));
    private static final BiConsumer<UriBuilder, PullRequestStateFilter> STATE_FILTER_ENCODER =
            ((uriBuilder, state) -> uriBuilder.addQueryParam("state", state.name()));
    private static final BiConsumer<UriBuilder, PullRequestsOrder> ORDER_ENCODER =
            ((uriBuilder, order) -> uriBuilder.addQueryParam("order", order.name()));

    /**
     * Creates client able to perform Stash REST API calls
     *
     * @param httpExecutor Http executor responsible for performing http requests
     * @param pageLimit    Number of values retrieved for {@code isUserKey/isRepositoryKey} methods
     */
    public StashClientImpl(HttpExecutor httpExecutor, int pageLimit) {
        this.httpExecutor = httpExecutor;
        this.pageLimit = pageLimit;
    }

    public StashClientImpl(HttpExecutor httpExecutor) {
        this(httpExecutor, StashRestClientProperties.STASH_REST_PAGE_LIMIT);
    }

    @Nonnull
    @Override
    public Page<User> getUsers(@Nullable final String filterForUsername, final long start, final long limit) {

        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/admin/users")
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                .addQueryParam("filter", filterForUsername);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(userParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<Project> getAccessibleProjects(final long start, final long limit) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/projects")
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(projectParser()).apply(jsonElement);
    }


    @Nonnull
    @Override
    public Page<Repository> getRepositories(@Nullable final String projectName, @Nullable final String query,
                                            final long start, final long limit) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/repos")
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                .encodeQueryParam(blankToNull(projectName), (builder, pk) -> builder.addQueryParam("projectname", pk))
                .encodeQueryParam(blankToNull(query), (builder, q) -> builder.addQueryParam("name", q));

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(repositoryParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<Repository> getProjectRepositories(@Nonnull final String projectKey, final long start, final long limit) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/projects/%s/repos", projectKey)
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(repositoryParser()).apply(jsonElement);
    }

    @Override
    @Nullable
    public Repository getRepository(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return expectingRestError(() -> {
            final UriBuilder uriBuilder =
                    UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s", projectKey, repositorySlug);

            JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);
            return repositoryParser().apply(jsonElement);
        }, HTTP_NOT_FOUND, null);
    }

    @Nonnull
    @Override
    public Page<Branch> getRepositoryBranches(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                              @Nullable final String query, final long start, final long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/branches", projectKey, repositorySlug)
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .addQueryParam("details", "true")
                        .addQueryParam("orderBy", "MODIFICATION")
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                        .encodeQueryParam(blankToNull(query), (builder, q) -> builder.addQueryParam("filterText", q));

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(branchParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<Tag> getRepositoryTags(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                       @Nullable final String query, final long start, final long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/tags", projectKey, repositorySlug)
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .addQueryParam("orderBy", "MODIFICATION")
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                        .encodeQueryParam(blankToNull(query), (builder, q) -> builder.addQueryParam("filterText", q));

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(tagParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<MirrorServer> getRepositoryMirrors(long repositoryId, final long start, final long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/mirroring/latest/repos/%s/mirrors", String.valueOf(repositoryId))
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);
        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);
        return pageParser(mirrorParser()).apply(jsonElement);
    }

    @Nullable
    @Override
    public Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return expectingRestError(() -> {
            final UriBuilder uriBuilder =
                    UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/branches/default", projectKey, repositorySlug);

            JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

            return branchParser().apply(jsonElement);
        }, HTTP_NOT_FOUND, null);
    }

    @Nonnull
    @Override
    public Page<RepositorySshKey> getRepositoryKeys(@Nonnull final String projectKey,
                                                    @Nonnull final String repositorySlug,
                                                    final long start, final long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/keys/1.0/projects/%s/repos/%s/ssh", projectKey, repositorySlug)
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(repositorySshKeyParser()).apply(jsonElement);
    }

    @Override
    public boolean addRepositoryKey(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                    @Nonnull final String publicKey, @Nullable final String keyLabel,
                                    @Nonnull final Permission keyPermission) {

        final StashRepositorySshKeyRequest payload = new StashRepositorySshKeyRequest(
                projectKey,
                repositorySlug,
                keyLabel,
                publicKey,
                keyPermission.name()
        );
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/keys/1.0/projects/%s/repos/%s/ssh", projectKey, repositorySlug);

        doRestCall(uriBuilder, POST, payload.toJson(), false);

        return true;
    }

    @Override
    public boolean isRepositoryKey(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                   @Nonnull final String publicKey) {
        if (null == blankToNull(publicKey)) {
            return false;
        }

        Integer pageStart = 0;
        do {
            Page<RepositorySshKey> keysPage = getRepositoryKeys(projectKey, repositorySlug, pageStart, pageLimit);
            for (RepositorySshKey key : keysPage.getValues()) {
                // key already exists on the server - so as this is the same public key - we don't upload it
                if (key.getText().equals(publicKey)) {
                    return true;
                }
            }
            pageStart = keysPage.getNextPageStart();
        }
        while (pageStart != null);

        return false;
    }

    @Nonnull
    @Override
    public Page<UserSshKey> getCurrentUserKeys(final long start,
                                               final long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/ssh/1.0/keys")
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(userSshKeyParser()).apply(jsonElement);
    }

    @Override
    public boolean isUserKey(@Nonnull final String publicKey) {
        if (null == blankToNull(publicKey)) {
            return false;
        }

        Integer pageStart = 0;
        do {
            Page<UserSshKey> keysPage = getCurrentUserKeys(pageStart, pageLimit);
            for (UserSshKey key : keysPage.getValues()) {
                // key already exists on the server - so as this is the same public key - we don't upload it
                if (key.getText().equals(publicKey)) {
                    return true;
                }
            }
            pageStart = keysPage.getNextPageStart();
        }
        while (pageStart != null);

        return false;
    }

    @Override
    public boolean addUserKey(@Nonnull final String publicKey, @Nullable String keyLabel) {

        final StashUserSshKeyRequest payload = new StashUserSshKeyRequest(keyLabel, publicKey);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/ssh/1.0/keys");

        doRestCall(uriBuilder, POST, payload.toJson(), false).getAsJsonObject().get("id").getAsLong();

        return true;
    }

    @Override
    public boolean removeUserKey(@Nonnull String publicKey) {
        Integer pageStart = 0;
        do {
            Page<UserSshKey> keysPage = getCurrentUserKeys(pageStart, pageLimit);
            for (UserSshKey key : keysPage.getValues()) {
                if (Objects.equal(key.getText(), publicKey)) {
                    return removeUserKey(key.getId());
                }
            }
            pageStart = keysPage.getNextPageStart();
        }
        while (pageStart != null);

        return false;
    }

    @Override
    public boolean removeUserKey(long keyId) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/ssh/1.0/keys/%s", Long.toString(keyId));

        doRestCall(uriBuilder, DELETE, null, false);

        return true;
    }

    @Override
    public boolean projectExists(@Nonnull String projectKey) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s", projectKey);

        // if the call succeeded, the project exists
        // if the resource was not found, the project doesn't exist
        return expectingRestError(() -> {
            doRestCall(uriBuilder, HttpMethod.GET, null, false);
            return true;
        }, HTTP_NOT_FOUND, false);
    }

    @Override
    public boolean createProject(@Nonnull String projectKey, @Nonnull String name, @Nonnull String type, @Nullable String description) {
        final StashCreateProjectKeyRequest payload = new StashCreateProjectKeyRequest(projectKey, name, type, description);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects");

        doRestCall(uriBuilder, POST, payload.toJson(), false);

        return true;
    }

    @Override
    public boolean updateProject(@Nonnull String projectKey, @Nonnull String newProjectKey,
        @Nonnull String name, @Nullable String description) {
        final StashUpdateProjectRequest payload = new StashUpdateProjectRequest(newProjectKey, name, description);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s", projectKey);

        doRestCall(uriBuilder, HttpMethod.PUT, payload.toJson(), false);

        return true;
    }

    @Override
    public Page<ProjectGroupPermission> getProjectGroupPermissions(@Nonnull String projectKey,
        @Nullable String filter, long start, long limit) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/projects/%s/permissions/groups", projectKey)
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                .addQueryParam("filter", filter);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(projectGroupPermissionParser()).apply(jsonElement);
    }

    @Override
    public boolean addProjectGroupPermission(@Nonnull String projectKey, @Nonnull String groupName,
        @Nonnull ProjectPermission permission) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/permissions/groups", projectKey)
                      .addQueryParam("permission", permission.name())
                      .addQueryParam("name", groupName);

        doRestCall(uriBuilder, PUT, null, false);

        return true;
    }

    @Override
    public Page<ProjectUserPermission> getProjectUserPermissions(@Nonnull String projectKey,
        @Nullable String filter, final long start, final long limit) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/projects/%s/permissions/users", projectKey)
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                .addQueryParam("filter", filter);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(projectUserPermissionParser()).apply(jsonElement);
    }

    @Override
    public boolean addProjectUserPermission(@Nonnull String projectKey, @Nonnull String userName,
        @Nonnull ProjectPermission permission) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/permissions/users", projectKey)
                      .addQueryParam("permission", permission.name())
                      .addQueryParam("name", userName);

        doRestCall(uriBuilder, HttpMethod.PUT, null, false);

        return true;
    }

    @Override
    public boolean createRepository(@Nonnull String projectKey, @Nonnull String name, @Nonnull String scmId, boolean forkable) {
        final StashCreateRepositoryRequest payload = new StashCreateRepositoryRequest(name, scmId, forkable);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos", projectKey);

        doRestCall(uriBuilder, POST, payload.toJson(), false);

        return true;

    }

    @Override
    public boolean deleteProject(@Nonnull String projectKey) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s", projectKey);

        doRestCall(uriBuilder, DELETE, null, false);

        return true;
    }

    @Override
    public boolean deleteRepository(@Nonnull String projectKey, @Nonnull String repositorySlug) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s", projectKey, repositorySlug);

        doRestCall(uriBuilder, DELETE, null, false);

        return true;
    }

    @Override
    @Nonnull
    public ImmutableMap<String, String> getStashApplicationProperties() {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/application-properties");

        JsonElement jsonElement = doRestCall(uriBuilder, GET, null, true);

        final ImmutableMap.Builder<String, String> resultBuilder = ImmutableMap.builder();
        if (jsonElement != null) {
            for (final Map.Entry<String, JsonElement> entry : jsonElement.getAsJsonObject().entrySet()) {
                resultBuilder.put(entry.getKey(), entry.getValue().getAsString());
            }
        }
        return resultBuilder.build();
    }

    @Override
    public Optional<Permission> getCurrentUserRepositoryPermission(@Nonnull String projectKey, @Nonnull String repositorySlug) {
        try {
            // only users with REPO_ADMIN permission can check other user's permissions to a repository
            final UriBuilder uriBuilder =
                    UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/permissions/users", projectKey, repositorySlug)
                            .addQueryParam("name", "admin");

            doRestCall(uriBuilder, GET, null, false);

            return Optional.of(Permission.REPO_ADMIN);
        } catch (StashUnauthorizedRestException e) {
            // swallow this exception
            log.debug("Exception when checking for REPO_ADMIN permission", e);
        }

        try {
            // only users with REPO_READ permission can see repository's default branch
            final UriBuilder uriBuilder =
                    UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/branches/default", projectKey, repositorySlug);

            doRestCall(uriBuilder, GET, null, false);

            return Optional.of(Permission.REPO_READ);
        } catch (StashUnauthorizedRestException e) {
            // swallow this exception
            log.debug("Exception when checking for REPO_READ permission", e);
        }

        return Optional.empty();
    }

    @Override
    public boolean addRepositoryUserPermission(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                               @Nonnull final String userName, @Nonnull final Permission permission) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/permissions/users", projectKey, repositorySlug)
                        .addQueryParam("name", userName)
                        .addQueryParam("permission", permission.toString());

        doRestCall(uriBuilder, PUT, null, false);

        return true;
    }

    @Nonnull
    @Override
    public PullRequestStatus createPullRequest(@Nonnull final String title, @Nullable final String description,
                                               @Nonnull final PullRequestRef fromRef, @Nonnull final PullRequestRef toRef,
                                               @Nonnull final Iterable<String> reviewers,
                                               @Nullable AvatarRequest avatarRequest) {
        final StashCreatePullReqRequest payload = new StashCreatePullReqRequest(title, description, fromRef, toRef, reviewers);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/pull-requests", toRef.getProjectKey(),
                        toRef.getRepositorySlug())
                        .encodeQueryParam(avatarRequest, AVATAR_REQUEST_ENCODER);

        final JsonElement jsonElement = doRestCall(uriBuilder, POST, payload.toJson(), false);

        return pullRequestStatusParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<PullRequestStatus> getPullRequestsByRepository(@Nonnull final String projectKey,
                                                               @Nonnull final String repositorySlug,
                                                               @Nullable final String branchName,
                                                               @Nullable final PullRequestDirection direction,
                                                               @Nullable final PullRequestStateFilter stateFilter,
                                                               @Nullable final PullRequestsOrder order,
                                                               final long start, final long limit,
                                                               @Nullable AvatarRequest avatarRequest) {
        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/pull-requests", projectKey, repositorySlug)
                .encodeQueryParam(start, START_PARAM_ENCODER)
                .encodeQueryParam(limit, LIMIT_PARAM_ENCODER)
                .addQueryParam("at", branchName)
                .encodeQueryParam(direction, DIRECTION_ENCODER)
                .encodeQueryParam(stateFilter, STATE_FILTER_ENCODER)
                .encodeQueryParam(order, ORDER_ENCODER)
                .encodeQueryParam(avatarRequest, AVATAR_REQUEST_ENCODER);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(pullRequestStatusParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public PullRequestStatus mergePullRequest(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                              final long pullRequestId, final long version,
                                              @Nullable AvatarRequest avatarRequest) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/pull-requests/%s/merge", projectKey,
                        repositorySlug, Long.toString(pullRequestId))
                        .addQueryParam("version", Long.toString(version));

        final JsonElement jsonElement = doRestCall(uriBuilder, POST, null, false);

        return pullRequestStatusParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public PullRequestMergeability canMergePullRequest(@Nonnull final String projectKey,
                                                       @Nonnull final String repositorySlug, final long pullRequestId) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/pull-requests/%s/merge", projectKey,
                        repositorySlug, Long.toString(pullRequestId));

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pullRequestMergeabilityParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Repository forkRepository(@Nonnull final String sourceProjectKey, @Nonnull final String sourceRepositorySlug,
                                     @Nonnull final String targetProjectKey, @Nonnull final String targetRepositorySlug) {
        final StashForkRepositoryRequest payload = new StashForkRepositoryRequest(targetRepositorySlug, targetProjectKey);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s", sourceProjectKey, sourceRepositorySlug);

        final JsonElement jsonElement = doRestCall(uriBuilder, POST, payload.toJson(), false);

        return repositoryParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Comment addPullRequestGeneralComment(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                                long pullRequestId, @Nonnull final String text) {
        final CreateCommentRequest payload = new CreateCommentRequest(text);

        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/projects/%s/repos/%s/pull-requests/%s/comments", projectKey,
                        repositorySlug, Long.toString(pullRequestId));

        final JsonElement jsonElement = doRestCall(uriBuilder, POST, payload.toJson(), false);

        return commentParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Task addTask(@Nonnull final TaskAnchor anchor, @Nonnull final String text) {
        final CreateTaskRequest payload = CreateTaskRequest.forAnchor(anchor, text);

        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/tasks");

        final JsonElement jsonElement = doRestCall(uriBuilder, POST, payload.toJson(), false);

        return taskParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Task updateTask(final long taskId, @Nullable final TaskState state, @Nullable final String text) {
        final UpdateTaskRequest payload = new UpdateTaskRequest(state, text);

        final UriBuilder uriBuilder = UriBuilder.forPath("/rest/api/1.0/tasks/%s", Long.toString(taskId));

        final JsonElement jsonElement = doRestCall(uriBuilder, PUT, payload.toJson(), false);

        return taskParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public ApplicationProperties getApplicationProperties() {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/api/1.0/application-properties");

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return applicationPropertiesParser().apply(jsonElement);
    }

    @Nonnull
    @Override
    public Page<Report> getReports(@Nonnull String projectKey, @Nonnull String repositorySlug, @Nonnull String revision,
                                   long start, long limit) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports",
                        projectKey, repositorySlug, revision)
                        .encodeQueryParam(start, START_PARAM_ENCODER)
                        .encodeQueryParam(limit, LIMIT_PARAM_ENCODER);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return pageParser(reportParser()).apply(jsonElement);
    }

    @Nonnull
    @Override
    public Optional<Report> getReport(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                      @Nonnull String revision, @Nonnull String key) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s",
                        projectKey, repositorySlug, revision, key);

        return expectingRestError(() -> {
            final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);
            return Optional.of(reportParser().apply(jsonElement));
        }, HTTP_NOT_FOUND, Optional.empty());
    }

    @Nonnull
    @Override
    public Report createReport(@Nonnull String projectKey, @Nonnull String repositorySlug,
                               @Nonnull String revision, @Nonnull Report report) {
        final ReportRequest payload = new ReportRequest(report);
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s",
                        projectKey, repositorySlug, revision, report.getKey());

        final JsonElement jsonElement = doRestCall(uriBuilder, PUT, payload.toJson(), false);

        return reportParser().apply(jsonElement);
    }

    @Override
    public void deleteReport(@Nonnull String projectKey, @Nonnull String repositorySlug,
                             @Nonnull String revision, @Nonnull String key) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s",
                        projectKey, repositorySlug, revision, key);

        doRestCall(uriBuilder, DELETE, null, false);
    }

    @Nonnull
    @Override
    public List<CodeAnnotation> getCodeAnnotations(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                   @Nonnull String revision) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/annotations", projectKey, repositorySlug, revision);

        final JsonElement jsonElement = doRestCall(uriBuilder, GET, null, false);

        return propertyParser("annotations", listParser(codeAnnotationParser())).apply(jsonElement);
    }

    @Override
    public boolean createCodeAnnotations(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                      @Nonnull String revision, @Nonnull String reportKey,
                                      @Nonnull Iterable<CodeAnnotation> codeAnnotations) {
        final CodeAnnotationsRequest payload = new CodeAnnotationsRequest(codeAnnotations);
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s/annotations",
                        projectKey, repositorySlug, revision, reportKey);

        return expectingRestError(() -> {
            doRestCall(uriBuilder, POST, payload.toJson(), false);
            return true;
        }, HTTP_ENTITY_TOO_LARGE, false);
    }

    @Override
    public boolean updateCodeAnnotation(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                     @Nonnull String revision, @Nonnull String reportKey,
                                     @Nonnull String externalId, @Nonnull CodeAnnotation codeAnnotation) {
        final CodeAnnotationRequest payload = new CodeAnnotationRequest(codeAnnotation);
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s/annotations/%s",
                        projectKey, repositorySlug, revision, reportKey, externalId);

        return expectingRestError(() -> {
            doRestCall(uriBuilder, PUT, payload.toJson(), false);
            return true;
        }, HTTP_ENTITY_TOO_LARGE, false);
    }

    @Override
    public void deleteCodeAnnotation(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                     @Nonnull String revision, @Nonnull String reportKey, @Nonnull String externalId) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s/annotations",
                        projectKey, repositorySlug, revision, reportKey)
                        .addQueryParam("externalId", externalId);

        doRestCall(uriBuilder, DELETE, null, false);
    }

    @Override
    public void deleteCodeAnnotations(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                      @Nonnull String revision, @Nonnull String reportKey) {
        final UriBuilder uriBuilder =
                UriBuilder.forPath("/rest/insights/1.0/projects/%s/repos/%s/commits/%s/reports/%s/annotations",
                        projectKey, repositorySlug, revision, reportKey);

        doRestCall(uriBuilder, DELETE, null, false);
    }

    @Nullable
    protected JsonElement doRestCall(@Nonnull UriBuilder requestUrlBuilder, @Nonnull HttpMethod methodType,
                                     @Nullable JsonElement requestJson, boolean anonymousCall) throws StashException {
        return doRestCall(requestUrlBuilder.build(), methodType, requestJson, anonymousCall);
    }

    @Nullable
    protected JsonElement doRestCall(@Nonnull String requestUrl, @Nonnull HttpMethod methodType,
                                     @Nullable JsonElement requestJson, boolean anonymousCall) throws StashException {

        String requestData = requestJson != null ? requestJson.toString() : null;
        if (log.isTraceEnabled()) {
            log.trace(String.format("doRestCall request: methodType=%s; requestUrl='%s'; requestJson='%s'; anonymous?=%s", methodType, requestUrl, requestJson, anonymousCall));
        }

        try {
            return httpExecutor.execute(new HttpRequest(requestUrl, methodType, requestData, anonymousCall),
                    response -> {
                        String responseString = null;
                        if (response.getBodyStream() != null) {
                            responseString = CharStreams.toString(new BufferedReader(response.getBodyReader(StandardCharsets.UTF_8.name())));
                        }
                        if (log.isTraceEnabled()) {
                            log.trace(String.format("doRestCall response: code=%d; response='%s'", response.getStatusCode(), responseString));
                        }

                        if (response.isSuccessful()) {
                            if (responseString != null) {
                                try {
                                    JsonElement jsonElement = new JsonParser().parse(responseString);
                                    return jsonElement != null && !jsonElement.isJsonNull() ? jsonElement : null;
                                } catch (JsonSyntaxException e) {
                                    throw createStashRestException(response, toErrors("Failed to parse response: " + e.getMessage()), responseString);
                                }
                            }
                            return null;
                        } else {
                            List<StashError> errors;
                            try {
                                if (responseString != null) {
                                    JsonElement jsonElement = new JsonParser().parse(responseString);
                                    if (jsonElement != null && jsonElement.isJsonObject()) {
                                        errors = errorsParser().apply(jsonElement);
                                    } else {
                                        errors = toErrors("Request to Stash failed. Returned with " + response.getStatusCode() + ". Response body is empty.");
                                    }
                                } else {
                                    errors = new ArrayList<>();
                                }
                            } catch (JsonSyntaxException entityException) {
                                errors = toErrors("Request to Stash failed. Returned with " + response.getStatusCode());
                            }
                            throw createStashRestException(response, errors, responseString);
                        }
                    });
        } catch (RuntimeException e) {
            if (log.isTraceEnabled()) {
                log.trace(e, e);
            }
            throw e;
        }
    }

    private StashRestException createStashRestException(HttpResponse response, List<StashError> errors, String responseString) {
        switch (response.getStatusCode()) {
            case HTTP_UNAUTHORIZED:
                return new StashUnauthorizedRestException(errors, response.getStatusCode(), response.getStatusMessage(), responseString);
            default:
                return new StashRestException(errors, response.getStatusCode(), response.getStatusMessage(), responseString);
        }
    }

    private static String blankToNull(@Nullable final String maybeBlank) {
        return (maybeBlank == null || maybeBlank.isEmpty() || maybeBlank.trim().isEmpty()) ? null : maybeBlank.trim();
    }

    private static <T> T expectingRestError(Supplier<T> call, int exceptionalHttpStatus, T valueForHttpStatus) {
        try {
            return call.get();
        } catch (StashRestException e) {
            if (e.getStatusCode() == exceptionalHttpStatus) {
                return valueForHttpStatus;
            } else {
                throw e;
            }
        }
    }
}
