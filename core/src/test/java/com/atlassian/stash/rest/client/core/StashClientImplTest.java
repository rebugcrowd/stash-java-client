package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.api.AvatarRequest;
import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Report;
import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskState;
import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpMethod;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import com.jayway.jsonassert.JsonAssert;
import com.jayway.jsonpath.JsonPath;
import junit.framework.AssertionFailedError;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atlassian.stash.rest.client.api.EntityMatchers.applicationProperties;
import static com.atlassian.stash.rest.client.api.EntityMatchers.branch;
import static com.atlassian.stash.rest.client.api.EntityMatchers.codeAnnotation;
import static com.atlassian.stash.rest.client.api.EntityMatchers.comment;
import static com.atlassian.stash.rest.client.api.EntityMatchers.mirrorServer;
import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.projectGroupPermission;
import static com.atlassian.stash.rest.client.api.EntityMatchers.projectUserPermission;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestMergeability;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestParticipant;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestRef;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestStatus;
import static com.atlassian.stash.rest.client.api.EntityMatchers.report;
import static com.atlassian.stash.rest.client.api.EntityMatchers.reportDataEntry;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.EntityMatchers.stashError;
import static com.atlassian.stash.rest.client.api.EntityMatchers.tag;
import static com.atlassian.stash.rest.client.api.EntityMatchers.task;
import static com.atlassian.stash.rest.client.api.EntityMatchers.user;
import static com.atlassian.stash.rest.client.api.EntityMatchers.userSshKey;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestDirection.OUTGOING;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestStateFilter.OPEN;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestsOrder.NEWEST;
import static com.atlassian.stash.rest.client.core.TestData.ADD_REPO_USER_PERM_ERROR_USER_NOT_FOUND;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_ENTITY_TOO_LARGE;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StashClientImplTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private HttpExecutor httpExecutor;
    private StashClientImpl stashClient;

    @Before
    public void setUp() throws Exception {
        stashClient = new StashClientImpl(httpExecutor);
    }

    @Test
    public void testGetAccessibleProjects() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.PROJECTS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<Project> projectsPage = stashClient.getAccessibleProjects(0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects?start=0&limit=10"));

        assertThat(projectsPage, page(Project.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(projectsPage.getValues().get(0), project()
                .name(is("My Cool Project"))
                .key(is("PRJ"))
                .selfUrl(is("http://link/to/project"))
                .build()
        );
    }

    @Test
    public void testRemoveUserKeyById() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey(123);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));

        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), is("/rest/ssh/1.0/keys/123"));
        assertThat(result, is(true));
    }

    @Test
    public void testRemoveUserKeyByPublicKeyAndLabel() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.USER_KEYS)
                .nextResponse(HTTP_OK, TestData.USER_KEYS_2)
                .nextResponse(HTTP_NO_CONTENT);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey("ssh-rsa EEEEEE... he@127.0.0.1");

        // then
        verify(httpExecutor, times(3)).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest deletingRequest = captor.getAllValues().get(2);
        assertThat(deletingRequest.getUrl(), is("/rest/ssh/1.0/keys/3"));
        assertThat(deletingRequest.getMethod(), equalTo(HttpMethod.DELETE));
        assertThat(result, equalTo(true));
    }

    @Test
    public void testRemoveUserKeyByPublicKeyAndLabel_shouldReturnFalseForNonExistingKeys() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.USER_KEYS)
                .nextResponse(HTTP_OK, TestData.USER_KEYS_2);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey("ssh-rsa XXXXXX... he@127.0.0.1");

        // then
        verify(httpExecutor, times(2)).execute(captor.capture(), any(HttpResponseProcessor.class)); // in this situation implementation asked for two pages but there was no such key
        assertThat(result, equalTo(false));
    }

    @Test
    public void testProjectExists() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.PROJECT);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.projectExists("PRJ");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ"));

        assertThat(result, is(true));
    }

    @Test
    public void testProjectExists_nonexisting() {
        // given
        // error json is the same, so use the anchor not found test data
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND,
            TestData.CREATE_TASK_ERROR_ANCHOR_NOT_FOUND);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.projectExists("PRJ");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ"));

        assertThat(result, is(false));
    }

    @Test
    public void testProjectExists_should401WhenUnauthorized() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_UNAUTHORIZED, TestData.REPO_CREATE_401);

        // when
        try {
            stashClient.projectExists("PRJ");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_UNAUTHORIZED));
        }
    }

    @Test
    public void testCreateProject() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.createProject("KEY", "NAME", "NORMAL", "DESC");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects"));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.key", is("KEY"))
                .assertThat("$.name", is("NAME"))
                .assertThat("$.type", is("NORMAL"))
                .assertThat("$.description", is("DESC"));

        assertThat(result, is(true));
    }

    @Test
    public void testCreateProject_should409OnConflict() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CONFLICT, TestData.REPO_CREATE_409);

        // when
        try {
            stashClient.createProject("KEY", "NAME", "NORMAL", "DESCRIPTION");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
        }
    }

    @Test
    public void testCreateProject_should401WhenUnauthorized() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_UNAUTHORIZED, TestData.REPO_CREATE_401);

        // when
        try {
            stashClient.createProject("KEY", "NAME", "NORMAL", "DESCRIPTION");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_UNAUTHORIZED));
        }
    }

    @Test
    public void testUpdateProject() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.updateProject("KEY", "NEWKEY", "NAME", "DESCRIPTION");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY"));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.key", is("NEWKEY"))
                .assertThat("$.name", is("NAME"))
                .assertThat("$.description", is("DESCRIPTION"));

        assertThat(result, is(true));
    }

    @Test
    public void testUpdateProject_should401WhenUnauthorized() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_UNAUTHORIZED, TestData.REPO_CREATE_401);

        // when
        try {
            stashClient.updateProject("KEY", "NEWKEY", "NAME", "DESCRIPTION");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_UNAUTHORIZED));
        }
    }

    @Test
    public void testGetProjectGroupPermissions() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.PROJECT_GROUP_PERMISSIONS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<ProjectGroupPermission> result = stashClient
                .getProjectGroupPermissions("KEY", null, 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY/permissions/groups?start=0&limit=25"));

        assertThat(result, page(ProjectGroupPermission.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );

        assertThat(result.getValues().get(0), projectGroupPermission()
                .groupName(is("GROUP"))
                .permission(is(ProjectPermission.PROJECT_READ))
                .build()
        );
    }

    @Test
    public void testGetProjectGroupPermissionsFilterParam() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.PROJECT_GROUP_PERMISSIONS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<ProjectGroupPermission> result = stashClient
                .getProjectGroupPermissions("KEY", "GROUP", 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY/permissions/groups?start=0&limit=25&filter=GROUP"));

        assertThat(result, page(ProjectGroupPermission.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );

        assertThat(result.getValues().get(0), projectGroupPermission()
                .groupName(is("GROUP"))
                .permission(is(ProjectPermission.PROJECT_READ))
                .build()
        );
    }

    @Test
    public void testAddProjectGroupPermission() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.addProjectGroupPermission("KEY", "GROUP",
            ProjectPermission.PROJECT_READ);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY/permissions/groups?"
                    + "permission=PROJECT_READ&name=GROUP"));

        assertThat(result, is(true));
    }

    @Test
    public void testGetProjectUserPermissions() throws Exception
    {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.PROJECT_USER_PERMISSIONS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<ProjectUserPermission> result = stashClient.getProjectUserPermissions("KEY", "fo", 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY/permissions/users?start=0&limit=25&filter=fo"));

        assertThat(result, page(ProjectUserPermission.class)
                .size(is(2))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );

        assertThat(result.getValues().get(0), projectUserPermission()
                .userName(is("footer"))
                .permission(is(ProjectPermission.PROJECT_ADMIN))
                .build()
        );

        assertThat(result.getValues().get(1), projectUserPermission()
            .userName(is("foo"))
            .permission(is(ProjectPermission.PROJECT_READ))
            .build()
        );
    }

    @Test
    public void testAddProjectUserPermission() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.addProjectUserPermission("KEY", "USER",
            ProjectPermission.PROJECT_ADMIN);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/KEY/permissions/users?"
                    + "permission=PROJECT_ADMIN&name=USER"));

        assertThat(result, is(true));
    }

    @Test
    public void testDeleteProject() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.deleteProject("PRJ");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));

        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), is("/rest/api/1.0/projects/PRJ"));
        assertThat(result, is(true));
    }


    @Test
    public void testCreateRepository() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.createRepository("KEY", "SLUG", "git", false);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest request = captor.getValue();
        String payload = request.getPayload();
        String url = request.getUrl();

        assertThat(url, is("/rest/api/1.0/projects/KEY/repos"));
        assertThat(JsonPath.read(payload, "$.name"), is("SLUG"));
        assertThat(JsonPath.read(payload, "$.scmId"), is("git"));
        assertThat(JsonPath.<Boolean>read(payload, "$.forkable"), is(false));
        assertThat(result, is(true));
    }

    @Test
    public void testDeleteRepository() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.deleteRepository("PRJ", "SLUG");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));

        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), is("/rest/api/1.0/projects/PRJ/repos/SLUG"));
        assertThat(result, is(true));
    }

    @Test
    public void testGetRepositories() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPOS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<Repository> repositoriesPage = stashClient.getRepositories(null, null, 0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/repos?start=0&limit=10"));

        assertThat(repositoriesPage, page(Repository.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(repositoriesPage.getValues().get(0), repository()
                .slug(is("my-repo"))
                .name(is("My repo"))
                .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                .selfUrl(is("http://link/to/repository"))
                .project(project()
                        .name(is("My Cool Project"))
                        .key(is("PRJ"))
                        .selfUrl(is("http://link/to/project"))
                        .build()
                )
                .build()
        );
    }

    @Test
    public void testGetProjectRepositories() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPOS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<Repository> repositoriesPage = stashClient.getProjectRepositories("PRJ", 0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos?start=0&limit=10"));

        assertThat(repositoriesPage, page(Repository.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(repositoriesPage.getValues().get(0), repository()
                .slug(is("my-repo"))
                .name(is("My repo"))
                .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                .selfUrl(is("http://link/to/repository"))
                .project(project()
                        .name(is("My Cool Project"))
                        .key(is("PRJ"))
                        .selfUrl(is("http://link/to/project"))
                        .build()
                )
                .build()
        );
    }

    @Test
    public void testGetRepository() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPO_MY_REPO);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/my-repo"));

        assertThat(repository, repository()
                .slug(is("my-repo"))
                .name(is("My repo"))
                .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                .selfUrl(is("http://link/to/repository"))
                .project(project()
                        .name(is("My Cool Project"))
                        .key(is("PRJ"))
                        .selfUrl(is("http://link/to/project"))
                        .isPublic(is(true))
                        .isPersonal(is(false))
                        .build()
                )
                .origin(CoreMatchers.nullValue())
                .build()
        );
    }

    @Test
    public void testGetRepositoryPersonalForked() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPO_PERSONAL_FORKED);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/my-repo"));

        assertThat(repository, repository()
                .origin(repository()
                        .name(is("confluence"))
                        .project(project()
                                .name(is("Confluence"))
                                .build()
                        )
                        .build()
                )
                .project(project()
                        .isPersonal(is(true))
                        .build()
                )
                .build()
        );
    }

    @Test
    public void testGetRepositoryIfNotExists() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND, "");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Repository repository = stashClient.getRepository("PRJ", "NON_EXISTING_REPO");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/NON_EXISTING_REPO"));

        assertThat(repository, nullValue());
    }

    @Test
    public void testGetRepositoryBranches() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.BRANCHES);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<Branch> branchesPage = stashClient.getRepositoryBranches("PRJ", "my-repo", null, 0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/my-repo/branches?start=0&details=true&orderBy=MODIFICATION&limit=10"));

        assertThat(branchesPage, page(Branch.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(branchesPage.getValues().get(0), branch()
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetRepositoryTags() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.TAGS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<Tag> tagPage = stashClient.getRepositoryTags("PRJ", "my-repo", null, 0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/my-repo/tags?start=0&orderBy=MODIFICATION&limit=10"));

        assertThat(tagPage, page(Tag.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(tagPage.getValues().get(0), tag()
                .id(is("release-2.0.0"))
                .displayId(is("refs/tags/release-2.0.0"))
                .latestChangeset(is("8d351a10fb428c0c1239530256e21cf24f136e73"))
                .latestCommit(is("8d351a10fb428c0c1239530256e21cf24f136e73"))
                .hash(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                .build()
        );
    }

    @Test
    public void testGetRepositoryDefaultBranch() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.BRANCH);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Branch branch = stashClient.getRepositoryDefaultBranch("PRJ", "my-repo");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PRJ/repos/my-repo/branches/default"));

        assertThat(branch, branch()
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetUserKeys() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.USER_KEYS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<UserSshKey> keysPage = stashClient.getCurrentUserKeys(0, 10);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/ssh/1.0/keys?start=0&limit=10"));

        assertThat(keysPage, page(UserSshKey.class)
                .size(is(2))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(keysPage.getValues().get(0), userSshKey()
                .id(is(1L))
                .text(is("ssh-rsa AAAAB3... me@127.0.0.1"))
                .label(is("me@127.0.0.1"))
                .build()
        );
        assertThat(keysPage.getValues().get(1), userSshKey()
                .id(is(2L))
                .text(is("AAAAB3...bo2c="))
                .label(nullValue(String.class))
                .build()
        );
    }

    @Test
    public void testIsUserKey() throws Exception {
        // given
        stashClient = new StashClientImpl(httpExecutor, 2); // set page limit to 2 to limit number keys to iterate thru
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // first response
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.USER_KEYS_2);
        // second response
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.USER_KEYS);

        // when
        // check key from seconds response
        boolean isKey = stashClient.isUserKey("ssh-rsa AAAAB3... me@127.0.0.1");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/ssh/1.0/keys?start=0&limit=2"));

        assertThat(isKey, is(true));
    }

    @Test(expected = StashRestException.class)
    public void testHttpErrorCausesStashRestException() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_BAD_REQUEST, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    @Test(expected = StashUnauthorizedRestException.class)
    public void testHttpError401CausesStashUnauthorizedRestException() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_UNAUTHORIZED, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    @Test(expected = StashRestException.class)
    public void testGetCurrentUserRepositoryPermissionRepositoryNotExists() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND, "");

        // when
        stashClient.getCurrentUserRepositoryPermission("FOO", "bar");
    }

    @Test
    public void testAddRepositoryUserPermission() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");

        // when
        stashClient.addRepositoryUserPermission("PRJ", "rep", "alice", Permission.REPO_READ);

        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url,
                is("/rest/api/1.0/projects/PRJ/repos/rep/permissions/users?name=alice&permission=REPO_READ"));
    }

    @Test
    public void testAddRepositoryUserPermission_should404WhenUserNotFound() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND, ADD_REPO_USER_PERM_ERROR_USER_NOT_FOUND);

        // when
        try {
            stashClient.addRepositoryUserPermission("PRJ", "rep", "phantom", Permission.REPO_READ);
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_NOT_FOUND));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("No such users: phantom"))
                    .build()));
        }
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasRepoAdminPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, "{message: 'Has REPO_ADMIN permission'}");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/FOO/repos/bar/permissions/users?name=admin"));

        assertThat(permission, equalTo(Optional.of(Permission.REPO_ADMIN)));
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasRepoReadPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_UNAUTHORIZED, "{error : [{message:'No REPO_ADMIN permission'}]}")
                .nextResponse(HTTP_OK, "{message: 'Has REPO_READ permission'}");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        verify(httpExecutor, times(2)).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/FOO/repos/bar/branches/default"));

        assertThat(permission, equalTo(Optional.of(Permission.REPO_READ)));
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasNoPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_UNAUTHORIZED, "{error : [{message:'No REPO_ADMIN permission'}]}")
                .nextResponse(HTTP_UNAUTHORIZED, "{error : [{message:'No REPO_READ permission'}]}");
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        verify(httpExecutor, times(2)).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/FOO/repos/bar/branches/default"));

        assertThat(permission, equalTo(Optional.<Permission>empty()));
    }

    @Test
    public void testCreatePullRequestStash_3_0() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final PullRequestRef fromRef = new PullRequestRef(
                "FROM-REPO-SLUG", "from-repo", "FROM-PROJECT-KEY", "From Project", "refs/heads/FROM-REF-ID");
        final PullRequestRef toRef = new PullRequestRef(
                "TO-REPO-SLUG", "to-repo", "TO-PROJECT-KEY", "To Project", "refs/heads/TO-REF-ID");
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQ_CREATE_STASH_3_0);

        // when
        final PullRequestStatus result = stashClient.createPullRequest("TITLE", "DESCRIPTION", fromRef,
                toRef, Arrays.asList("joe", "charlie", "bob"),
                AvatarRequest.builder().size(32).absolute(true).secure(true).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat("creating PR in the toRef project/repo", url,
                is("/rest/api/1.0/projects/TO-PROJECT-KEY/repos/TO-REPO-SLUG/pull-requests?avatarSize=32" +
                        "&avatarScheme=https&avatarUrlMode=absolute"));
        String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.title"), is("TITLE"));
        assertThat(JsonPath.read(payload, "$.description"), is("DESCRIPTION"));
        assertThat(JsonPath.read(payload, "$.fromRef.id"), is("refs/heads/FROM-REF-ID"));
        assertThat(JsonPath.read(payload, "$.fromRef.repository.slug"), is("FROM-REPO-SLUG"));
        assertThat(JsonPath.read(payload, "$.fromRef.repository.project.key"), is("FROM-PROJECT-KEY"));
        assertThat(JsonPath.read(payload, "$.toRef.id"), is("refs/heads/TO-REF-ID"));
        assertThat(JsonPath.read(payload, "$.toRef.repository.slug"), is("TO-REPO-SLUG"));
        assertThat(JsonPath.read(payload, "$.toRef.repository.project.key"), is("TO-PROJECT-KEY"));
        assertThat(JsonPath.read(payload, "$.reviewers[0].user.name"), is("joe"));
        assertThat(JsonPath.read(payload, "$.reviewers[1].user.name"), is("charlie"));
        assertThat(JsonPath.read(payload, "$.reviewers[2].user.name"), is("bob"));

        assertThat("scalar properties match", result, pullRequestStatus()
                .id(is(101L))
                .version(is(103L))
                .title(is("TITLE"))
                .description(is(Optional.of("DESCRIPTION")))
                .url(is("http://127.0.0.1:7990/stash/projects/TO-PROJECT-KEY/repos/to-repo-slug/pull-requests/121"))
                .mergeOutcome(is(Optional.empty()))
                .lastUpdated(is(1481829548195L))
                .commentCount(is(Optional.empty()))
                .outstandingTaskCount(is(Optional.empty()))
                .resolvedTaskCount(is(Optional.empty()))
                .build());

        assertThat("author matches", result.getAuthor(), is(pullRequestParticipant()
                .name(is("admin"))
                .displayName(is("Administrator"))
                .slug(is("admin-slug"))
                .avatarUrl(is("https://secure.gravatar.com/avatar/6bb79caa69a2a1f4386911cb1507c233.jpg?s=32&d=mm"))
                .approved(is(false))
                .status(is(nullValue()))
                .role(is("AUTHOR"))
                .build()));

        assertThat("reviewers match", result.getReviewers(), contains(
                pullRequestParticipant()
                        .name(is("joe"))
                        .displayName(is("Joe"))
                        .slug(is("joe-slug"))
                        .avatarUrl(is("https://secure.gravatar.com/avatar/6bb79caa69a2a1f4386911cb1507c233.jpg?s=32&d=mm"))
                        .approved(is(false))
                        .status(is(nullValue()))
                        .role(is("REVIEWER"))
                        .build(),
                pullRequestParticipant()
                        .name(is("charlie"))
                        .displayName(is("Charlie"))
                        .slug(is("charlie-slug"))
                        .avatarUrl(is("/users/charlie-slug/avatar.png?s=32&v=140982701100"))
                        .approved(is(false))
                        .status(is(nullValue()))
                        .role(is("REVIEWER"))
                        .build(),
                pullRequestParticipant()
                        .name(is("bob"))
                        .displayName(is("Bob"))
                        .slug(is("bob-slug"))
                        .avatarUrl(nullValue())
                        .approved(is(false))
                        .status(is(nullValue()))
                        .role(is("REVIEWER"))
                        .build()));

        assertThat("from ref matches", result.getFromRef(), is(pullRequestRef()
                .repositorySlug(is("FROM-REPO-SLUG"))
                .repositoryName(is("from-repo"))
                .projectKey(is("FROM-PROJECT-KEY"))
                .projectName(is("From Project"))
                .id(is("refs/heads/FROM-REF-ID"))
                .displayId(is("FROM-REF-ID"))
                .build()));

        assertThat("to ref matches", result.getToRef(), is(pullRequestRef()
                .repositorySlug(is("TO-REPO-SLUG"))
                .repositoryName(is("to-repo"))
                .projectKey(is("TO-PROJECT-KEY"))
                .projectName(is("To Project"))
                .id(is("refs/heads/TO-REF-ID"))
                .displayId(is("TO-REF-ID"))
                .build()));
    }

    @Test
    public void testCreatePullRequestBBS_4_11() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final PullRequestRef fromRef = new PullRequestRef(
                "FROM-REPO-SLUG", "from-repo", "FROM-PROJECT-KEY", "From Project", "refs/heads/FROM-REF-ID");
        final PullRequestRef toRef = new PullRequestRef(
                "TO-REPO-SLUG", "to-repo", "TO-PROJECT-KEY", "To Project", "refs/heads/TO-REF-ID");
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQ_CREATE_BBS_4_11);

        // when
        final PullRequestStatus result = stashClient.createPullRequest("TITLE", "DESCRIPTION", fromRef,
                toRef, Arrays.asList("joe", "charlie", "bob"), AvatarRequest.builder().size(64).secure(true).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat("creating PR in the toRef project/repo", url,
                is("/rest/api/1.0/projects/TO-PROJECT-KEY/repos/TO-REPO-SLUG/pull-requests?avatarSize=64" +
                        "&avatarScheme=https"));
        String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.title"), is("TITLE"));
        assertThat(JsonPath.read(payload, "$.description"), is("DESCRIPTION"));
        assertThat(JsonPath.read(payload, "$.fromRef.id"), is("refs/heads/FROM-REF-ID"));
        assertThat(JsonPath.read(payload, "$.fromRef.repository.slug"), is("FROM-REPO-SLUG"));
        assertThat(JsonPath.read(payload, "$.fromRef.repository.project.key"), is("FROM-PROJECT-KEY"));
        assertThat(JsonPath.read(payload, "$.toRef.id"), is("refs/heads/TO-REF-ID"));
        assertThat(JsonPath.read(payload, "$.toRef.repository.slug"), is("TO-REPO-SLUG"));
        assertThat(JsonPath.read(payload, "$.toRef.repository.project.key"), is("TO-PROJECT-KEY"));
        assertThat(JsonPath.read(payload, "$.reviewers[0].user.name"), is("joe"));
        assertThat(JsonPath.read(payload, "$.reviewers[1].user.name"), is("charlie"));
        assertThat(JsonPath.read(payload, "$.reviewers[2].user.name"), is("bob"));

        assertThat("scalar properties match", result, pullRequestStatus()
                .id(is(101L))
                .version(is(103L))
                .title(is("TITLE"))
                .description(is(Optional.of("DESCRIPTION")))
                .url(is("http://127.0.0.1:7990/stash/projects/TO-PROJECT-KEY/repos/to-repo-slug/pull-requests/121"))
                .mergeOutcome(is(Optional.empty()))
                .lastUpdated(is(1481829548195L))
                .commentCount(is(Optional.empty()))
                .outstandingTaskCount(is(Optional.empty()))
                .resolvedTaskCount(is(Optional.empty()))
                .build());

        assertThat("author matches", result.getAuthor(), is(pullRequestParticipant()
                .name(is("admin"))
                .displayName(is("Administrator"))
                .slug(is("admin-slug"))
                .avatarUrl(is("https://secure.gravatar.com/avatar/6bb79caa69a2a1f4386911cb1507c233.jpg?s=32&d=mm"))
                .approved(is(false))
                .status(is("UNAPPROVED"))
                .role(is("AUTHOR"))
                .build()));

        assertThat("reviewers match", result.getReviewers(), contains(
                pullRequestParticipant()
                        .name(is("joe"))
                        .displayName(is("Joe"))
                        .slug(is("joe-slug"))
                        .avatarUrl(is("https://secure.gravatar.com/avatar/6bb79caa69a2a1f4386911cb1507c233.jpg?s=32&d=mm"))
                        .approved(is(false))
                        .status(is("UNAPPROVED"))
                        .role(is("REVIEWER"))
                        .build()));

        assertThat("from ref matches", result.getFromRef(), is(pullRequestRef()
                .repositorySlug(is("FROM-REPO-SLUG"))
                .repositoryName(is("from-repo"))
                .projectKey(is("FROM-PROJECT-KEY"))
                .projectName(is("From Project"))
                .id(is("refs/heads/FROM-REF-ID"))
                .displayId(is("FROM-REF-ID"))
                .build()));

        assertThat("to ref matches", result.getToRef(), is(pullRequestRef()
                .repositorySlug(is("TO-REPO-SLUG"))
                .repositoryName(is("to-repo"))
                .projectKey(is("TO-PROJECT-KEY"))
                .projectName(is("To Project"))
                .id(is("refs/heads/TO-REF-ID"))
                .displayId(is("TO-REF-ID"))
                .build()));
    }

    @Test
    public void testCreatePullRequest_should401WhenUnauthorized() {
        // given
        final PullRequestRef fromRef = new PullRequestRef(
                "FROM-REPO-SLUG", "from-repo", "FROM-PROJECT-KEY", "From Project", "refs/heads/FROM-REF-ID");
        final PullRequestRef toRef = new PullRequestRef(
                "TO-REPO-SLUG", "to-repo", "TO-PROJECT-KEY", "To Project", "refs/heads/TO-REF-ID");
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_UNAUTHORIZED, TestData.PULL_REQ_CREATE_ERROR_UNATHORISED);

        // when
        try {
            stashClient.createPullRequest("TITLE", "DESC", fromRef, toRef, Collections.emptyList());
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_UNAUTHORIZED));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("You are not permitted to access this resource"))
                    .exceptionName(endsWith("AuthorisationException"))
                    .build()));
        }
    }

    @Test
    public void testCreatePullRequest_should409WhenDuplicate() {
        // given
        final PullRequestRef fromRef = new PullRequestRef(
                "FROM-REPO-SLUG", "from-repo", "FROM-PROJECT-KEY", "From Project", "refs/heads/FROM-REF-ID");
        final PullRequestRef toRef = new PullRequestRef(
                "TO-REPO-SLUG", "to-repo", "TO-PROJECT-KEY", "To Project", "refs/heads/TO-REF-ID");
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CONFLICT, TestData.PULL_REQ_CREATE_ERROR_DUP);

        // when
        try {
            stashClient.createPullRequest("TITLE", "DESC", fromRef, toRef, Collections.emptyList());
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("Only one pull request may be open for a given source and target branch"))
                    .exceptionName(endsWith("DuplicatePullRequestException"))
                    .build()));
        }
    }

    @Test
    public void testGetPullRequestsForBranchStash_3_0() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQS_FOR_BRANCH_STASH_3_0);

        // when
        final Page<PullRequestStatus> pullReqs = stashClient.getPullRequestsByRepository("PROJECT_1", "rep_1",
                "refs/heads/basic_branching", OUTGOING, OPEN, NEWEST, 0, 5,
                AvatarRequest.builder().size(16).absolute(true).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests?start=0&limit=5&" +
                "at=refs%2Fheads%2Fbasic_branching&direction=OUTGOING&state=OPEN&order=NEWEST&avatarSize=16" +
                "&avatarUrlMode=absolute"));

        assertThat(pullReqs, page(PullRequestStatus.class)
                .size(is(1))
                .limit(is(5))
                .values(Matchers.hasSize(1))
                .build()
        );
        final PullRequestStatus pullReq = pullReqs.getValues().iterator().next();

        assertThat("scalar properties match", pullReq, pullRequestStatus()
                .id(is(1L))
                .version(is(0L))
                .state(is("OPEN"))
                .title(is("Basic branching"))
                .description(is(Optional.of("a modification on branch basic_branching")))
                .url(is("http://127.0.0.1:7990/stash/projects/PROJECT_1/repos/rep_1/pull-requests/1"))
                .mergeOutcome(is(Optional.empty()))
                .lastUpdated(is(1482005729073L))
                .commentCount(is(Optional.of(1L)))
                .outstandingTaskCount(is(Optional.empty()))
                .resolvedTaskCount(is(Optional.empty()))
                .build());

        assertThat("author matches", pullReq.getAuthor(), is(pullRequestParticipant()
                .name(is("admin"))
                .displayName(is("Administrator"))
                .slug(is("admin"))
                .avatarUrl(is("http://127.0.0.1:7990/stash/s/en_GB/AUTHOR_AVATAR_URL"))
                .approved(is(false))
                .status(is(nullValue()))
                .role(is("AUTHOR"))
                .build()));

        assertThat("reviewers match", pullReq.getReviewers(), contains(
                pullRequestParticipant()
                        .name(is("user"))
                        .displayName(is("User"))
                        .slug(is("user"))
                        .avatarUrl(is("http://127.0.0.1:7990/stash/s/en_GB/USER_AVATAR_URL"))
                        .approved(is(false))
                        .status(is(nullValue()))
                        .role(is("REVIEWER"))
                        .build()));

        assertThat("from ref matches", pullReq.getFromRef(), is(pullRequestRef()
                .repositorySlug(is("rep_1"))
                .repositoryName(is("rep_1"))
                .projectKey(is("PROJECT_1"))
                .projectName(is("Project 1"))
                .id(is("refs/heads/basic_branching"))
                .displayId(is("basic_branching"))
                .build()));

        assertThat("to ref matches", pullReq.getToRef(), is(pullRequestRef()
                .repositorySlug(is("rep_1"))
                .repositoryName(is("rep_1"))
                .projectKey(is("PROJECT_1"))
                .projectName(is("Project 1"))
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .build()));
    }

    @Test
    public void testGetPullRequestsForBranchStash_3_3_validateTaskCounts() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQS_FOR_BRANCH_STASH_3_3);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Page<PullRequestStatus> pullReqs = stashClient.getPullRequestsByRepository("PROJECT_1", "rep_1",
                "refs/heads/basic_branching", OUTGOING, OPEN, NEWEST, 0, 5,
                AvatarRequest.builder().absolute(true).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests?start=0&limit=5&" +
                        "at=refs%2Fheads%2Fbasic_branching&direction=OUTGOING&state=OPEN&order=NEWEST&" +
                        "avatarUrlMode=absolute"));
        final PullRequestStatus pullReq = pullReqs.getValues().iterator().next();

        assertThat("scalar properties match", pullReq, pullRequestStatus()
                .commentCount(is(Optional.of(1L)))
                .state(is("OPEN"))
                .outstandingTaskCount(is(Optional.of(4L)))
                .resolvedTaskCount(is(Optional.of(12L)))
                .build());
    }

    @Test
    public void testGetPullRequestsForBranchBBS_4_0_validateTaskCounts() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQS_FOR_BRANCH_BBS_4_0);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Page<PullRequestStatus> pullReqs = stashClient.getPullRequestsByRepository("PROJECT_1", "rep_1",
                "refs/heads/basic_branching", OUTGOING, OPEN, NEWEST,0, 5,
                AvatarRequest.builder().size(10).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests?start=0&limit=5&" +
                        "at=refs%2Fheads%2Fbasic_branching&direction=OUTGOING&state=OPEN&order=NEWEST&avatarSize=10"));

        final PullRequestStatus pullReq = pullReqs.getValues().iterator().next();

        assertThat("scalar properties match", pullReq, pullRequestStatus()
                .commentCount(is(Optional.of(1L)))
                .state(is("OPEN"))
                .outstandingTaskCount(is(Optional.of(4L)))
                .resolvedTaskCount(is(Optional.of(12L)))
                .build());
    }

    @Test
    public void testGetPullRequestsForBranchBBS_4_10_validateMergeResult() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQS_FOR_BRANCH_BBS_4_10);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Page<PullRequestStatus> pullReqs = stashClient.getPullRequestsByRepository("PROJECT_1", "rep_1",
                "refs/heads/basic_branching", OUTGOING, OPEN, NEWEST, 0, 5,
                AvatarRequest.builder().size(10).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests?start=0&limit=5&" +
                        "at=refs%2Fheads%2Fbasic_branching&direction=OUTGOING&state=OPEN&order=NEWEST&avatarSize=10"));

        final PullRequestStatus pullReq = pullReqs.getValues().iterator().next();

        assertThat("pull request mergeOutcome", pullReq, pullRequestStatus()
                .mergeOutcome(is(Optional.of("CLEAN")))
                .state(is("OPEN"))
                .build());
    }

    @Test
    public void testGetPullRequestsForBranchBBS_4_10_validateMergeResultConflicted() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(HTTP_OK, TestData.PULL_REQS_FOR_BRANCH_BBS_4_10_MERGE_CONFLICT);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Page<PullRequestStatus> pullReqs = stashClient.getPullRequestsByRepository("PROJECT_1", "rep_1",
                "refs/heads/basic_branching", OUTGOING, OPEN, NEWEST, 0, 5,
                AvatarRequest.builder().size(10).build());

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests?start=0&limit=5&" +
                        "at=refs%2Fheads%2Fbasic_branching&direction=OUTGOING&state=OPEN&order=NEWEST&avatarSize=10"));

        final PullRequestStatus pullReq = pullReqs.getValues().iterator().next();

        assertThat("pull request mergeOutcome", pullReq, pullRequestStatus()
                .mergeOutcome(is(Optional.of("CONFLICTED")))
                .state(is("OPEN"))
                .build());
    }

    @Test
    public void testMergePullRequest() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.MERGE_PULL_REQ);

        // when
        final PullRequestStatus mergedPR = stashClient.mergePullRequest("PROJECT_1", "rep_1", 1, 0);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url,
                is("/rest/api/1.0/projects/PROJECT_1/repos/rep_1/pull-requests/1/merge?version=0"));

        assertThat("scalar properties match", mergedPR, pullRequestStatus()
                .id(is(1L))
                .version(is(1L))
                .state(is("MERGED"))
                .title(is("TITLE"))
                .description(is(Optional.of("DESCRIPTION")))
                .url(is("http://127.0.0.1:7990/stash/projects/PROJECT_1/repos/rep_1/pull-requests/1"))
                .mergeOutcome(is(Optional.empty()))
                .lastUpdated(is(1482090134897L))
                .commentCount(is(Optional.empty()))
                .outstandingTaskCount(is(Optional.empty()))
                .resolvedTaskCount(is(Optional.empty()))
                .build());

        assertThat("author matches", mergedPR.getAuthor(), is(pullRequestParticipant()
                .name(is("admin"))
                .displayName(is("Administrator"))
                .slug(is("admin"))
                .avatarUrl(is(nullValue()))
                .approved(is(false))
                .status(is(nullValue()))
                .role(is("AUTHOR"))
                .build()));

        assertThat("reviewers match", mergedPR.getReviewers(), hasSize(0));

        assertThat("from ref matches", mergedPR.getFromRef(), is(pullRequestRef()
                .repositorySlug(is("rep_1"))
                .repositoryName(is("rep_1"))
                .projectKey(is("PROJECT_1"))
                .projectName(is("Project 1"))
                .id(is("refs/heads/basic_branching"))
                .displayId(is("basic_branching"))
                .build()));

        assertThat("to ref matches", mergedPR.getToRef(), is(pullRequestRef()
                .repositorySlug(is("rep_1"))
                .repositoryName(is("rep_1"))
                .projectKey(is("PROJECT_1"))
                .projectName(is("Project 1"))
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .build()));
    }

    @Test
    public void testMergePullRequest_should409WhenOutOfDate() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CONFLICT, TestData.MERGE_PULL_REQ_ERROR_OUT_OF_DATE);

        // when
        try {
            stashClient.mergePullRequest("PROJECT_1", "rep_1", 1, 2);
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("You are attempting to modify a pull request based on out-of-date information."))
                    .exceptionName(endsWith("PullRequestOutOfDateException"))
                    .build()));
        }
    }

    @Test
    public void testCanMerge_cleanOnBBS4_9() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.CAN_MERGE_CLEAN_BBS_4_9);

        // when
        final PullRequestMergeability pullRequestMergeability =
                stashClient.canMergePullRequest("PROJECT", "repository", 65536);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url,
                is("/rest/api/1.0/projects/PROJECT/repos/repository/pull-requests/65536/merge"));

        assertThat(pullRequestMergeability, pullRequestMergeability()
                .outcome(is(Optional.empty()))
                .canMerge(is(TRUE))
                .conflicted(is(FALSE))
                .build());
    }

    @Test
    public void testCanMerge_conflictOnBBS4_10() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.CAN_MERGE_CONFLICTED_BBS_4_10);

        // when
        final PullRequestMergeability pullRequestMergeability =
                stashClient.canMergePullRequest("PROJ", "repo", 13);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url,
                is("/rest/api/1.0/projects/PROJ/repos/repo/pull-requests/13/merge"));

        assertThat(pullRequestMergeability, pullRequestMergeability()
                .outcome(is(Optional.of("CONFLICTED")))
                .canMerge(is(FALSE))
                .conflicted(is(TRUE))
                .build());
    }

    @Test
    public void testCanMerge_should409onDeclinedPR() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CONFLICT, TestData.CAN_MERGE_ERROR_PR_DECLINED_BBS_4_10);

        // when
        try {
            stashClient.canMergePullRequest("PROJ", "repo", 13);
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("This pull request has been declined and must be reopened before it can be merged."))
                    .exceptionName(endsWith("IllegalPullRequestStateException"))
                    .build()));
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
        }
    }

    @Test
    public void testForkRepository() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.FORK_REPO);

        // when
        final Repository repository = stashClient.forkRepository("PROJECT_1", "rep1", "PROJECT_2", "rep_1fork");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/projects/PROJECT_1/repos/rep1"));

        final String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.name"), is("rep_1fork"));
        assertThat(JsonPath.read(payload, "$.project.key"), is("PROJECT_2"));

        assertThat("Base repository attributes match", repository, repository()
                .slug(is("rep_1fork"))
                .name(is("rep_1fork"))
                .sshCloneUrl(is("ssh://git@127.0.0.1:7999/project_2/rep_1fork.git"))
                .httpCloneUrl(is("http://admin@127.0.0.1:7990/stash/scm/project_2/rep_1fork.git"))
                .selfUrl(is("http://127.0.0.1:7990/stash/projects/PROJECT_2/repos/rep_1fork/browse"))
                .build()
        );

        assertThat("Project matches", repository.getProject(), project()
                .name(is("Project 2"))
                .key(is("PROJECT_2"))
                .selfUrl(is("http://127.0.0.1:7990/stash/projects/PROJECT_2"))
                .isPublic(is(false))
                .isPersonal(is(false))
                .build()
        );

        assertThat("Origin matches", repository.getOrigin(), repository()
                .slug(is("rep1"))
                .name(is("Rep1"))
                .sshCloneUrl(is("ssh://git@127.0.0.1:7999/project_1/rep1.git"))
                .httpCloneUrl(is("http://admin@127.0.0.1:7990/stash/scm/project_1/rep1.git"))
                .selfUrl(is("http://127.0.0.1:7990/stash/projects/PROJECT_1/repos/rep1/browse"))
                .build()
        );

        assertThat("Origin project matches", repository.getOrigin().getProject(), project()
                .name(is("Project 1"))
                .key(is("PROJECT_1"))
                .selfUrl(is("http://127.0.0.1:7990/stash/projects/PROJECT_1"))
                .isPublic(is(false))
                .isPersonal(is(false))
                .build()
        );
    }

    @Test
    public void testForkRepository_should409OnConflict() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CONFLICT, TestData.FORK_REPO_ERROR_CONFLICT);

        // when
        try {
            stashClient.forkRepository("PROJECT_1", "rep1", "PROJECT_2", "rep_1fork");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_CONFLICT));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("This repository URL is already taken by 'rep_1fork' in 'Project 2'"))
                    .exceptionName(CoreMatchers.nullValue())
                    .build()));
        }
    }

    @Test
    public void testAddPullRequestGeneralComment() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.CREATE_COMMENT);

        // when
        final Comment comment = stashClient.addPullRequestGeneralComment("PRJ", "rep", 3, "some comment text.");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url,
                is("/rest/api/1.0/projects/PRJ/repos/rep/pull-requests/3/comments"));
        final String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.text"), is("some comment text."));

        assertThat(comment, comment()
                .id(is(2L))
                .version(is(0L))
                .text(is("some comment text."))
                .build());
    }

    @Test
    public void testAddTask() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.CREATE_TASK);
        final Comment anchor = new Comment(123, 34, "unimportant");

        // when
        final Task task = stashClient.addTask(anchor, "text of the new task");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/tasks"));

        final String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.anchor.id"), is(123));
        assertThat(JsonPath.read(payload, "$.anchor.type"), is("COMMENT"));
        assertThat(JsonPath.read(payload, "$.text"), is("text of the new task"));

        assertThat(task, task()
                .text(is("Example task"))
                .id(is(19L))
                .state(is("OPEN"))
                .commentAnchor(comment()
                        .id(is(21L))
                        .version(is(43L))
                        .text(is("An insightful general comment on a pull request."))
                        .build())
                .build());
    }

    @Test
    public void testAddTask_futureProofHypotheticalNewAnchorType() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.CREATE_TASK_FUTURE_PROOF_HYPOTHETICAL_ANCHOR_TYPE);
        final Comment anchor = new Comment(123, 34, "unimportant");

        // when
        final Task task = stashClient.addTask(anchor, "text of the new task");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/tasks"));

        final String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.anchor.id"), is(123));
        assertThat(JsonPath.read(payload, "$.anchor.type"), is("COMMENT"));
        assertThat(JsonPath.read(payload, "$.text"), is("text of the new task"));

        assertThat(task, task()
                .text(is("Example task"))
                .id(is(19L))
                .state(is("OPEN"))
                .anchor(CoreMatchers.notNullValue())
                .build());
    }

    @Test
    public void testAddTask_should404WhenAnchorNotFound() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND, TestData.CREATE_TASK_ERROR_ANCHOR_NOT_FOUND);
        final Comment anchor = new Comment(123, 34, "unimportant");

        // when
        try {
            stashClient.addTask(anchor, "text of the new task");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(HTTP_NOT_FOUND));
            assertThat(e.getErrors(), contains(stashError()
                    .message(is("The requested comment does not exist."))
                    .exceptionName(endsWith("NoSuchCommentException"))
                    .build()));
        }
    }

    @Test
    public void testUpdateTask() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.UPDATE_TASK);

        // when
        final Task task = stashClient.updateTask(123L, TaskState.RESOLVED, "new text");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/tasks/123"));

        final String payload = captor.getValue().getPayload();
        assertThat(JsonPath.read(payload, "$.state"), is("RESOLVED"));
        assertThat(JsonPath.read(payload, "$.text"), is("new text"));

        assertThat(task, task()
                .text(is("new text"))
                .id(is(22L))
                .state(is("RESOLVED"))
                .commentAnchor(comment()
                        .id(is(21L))
                        .version(is(43L))
                        .text(is("An insightful general comment on a pull request."))
                        .build())
                .build());
    }

    @Test
    public void testUpdateTask_resolveOnly() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.UPDATE_TASK);

        // when
        stashClient.updateTask(123L, TaskState.RESOLVED);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/tasks/123"));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.state", is("RESOLVED"))
                .assertNotDefined("$.text");
    }

    @Test
    public void testUpdateTask_updateTextOnly() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_CREATED, TestData.UPDATE_TASK);

        // when
        stashClient.updateTask(123L, "updating text only");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/tasks/123"));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.text", is("updating text only"))
                .assertNotDefined("$.state");
    }

    @Test
    public void testGetApplicationProperties() throws Exception {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.APPLICATION_PROPERTIES);

        // when
        final ApplicationProperties applicationProperties = stashClient.getApplicationProperties();

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        final String url = captor.getValue().getUrl();
        assertThat(url, is("/rest/api/1.0/application-properties"));

        assertThat(applicationProperties, applicationProperties()
                .version(is("3.3.5"))
                .buildNumber(is("3003005"))
                .buildDate(is("1419151242926"))
                .displayName(is("Stash"))
                .build());
    }

    @Test
    public void testGetRepositoryMirrors() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.MIRRORS);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<MirrorServer> mirrorsPage = stashClient.getRepositoryMirrors(1, 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/mirroring/latest/repos/1/mirrors?start=0&limit=25"));

        assertThat(mirrorsPage, page(MirrorServer.class)
                .size(is(4))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );

        assertThat(mirrorsPage.getValues().size(), is(4));

        assertThat(mirrorsPage.getValues().get(0), mirrorServer()
                .id(is("ID1"))
                .name(is("AU Mirror"))
                .selfUrl(is("https://bitbucket.example.com/rest/mirroring/latest/upstreamServers/abc/repos/35?jwt=abc"))
                .enabled(is(true))
                .build()
        );

        assertThat(mirrorsPage.getValues().get(3), mirrorServer()
                .id(is("ID4"))
                .name(is("US East Mirror2"))
                .enabled(is(false))
                .build()
        );

        for (int i = 1; i <= 3; ++i) {
            assertThat(mirrorsPage.getValues().get(i), mirrorServer().selfUrl(nullValue()).build());
        }


    }

    @Test
    public void testGetUsers() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.USER);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<User> usersPage = stashClient.getUsers("jcit", 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(), is("/rest/api/1.0/admin/users?start=0&limit=25&filter=jcit"));

        assertThat(usersPage, page(User.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(usersPage.getValues().get(0), user()
                .name(is("jcitizen"))
                .emailAddress(is("jane@example.com"))
                .id(is(101L))
                .displayName(is("Jane Citizen"))
                .active(is(true))
                .slug(is("jcitizen"))
                .type(is("NORMAL"))
                .directoryName(is("Bitbucket Internal Directory"))
                .deletable(is(true))
                .lastAuthenticationTimestamp(is(new Date(1368145580548L)))
                .mutableDetails(is(true))
                .mutableGroups(is(true))
                .build()
        );
    }

    @Test
    public void testGetUsersWithoutFilter() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.USER_WITHOUT_FILTER);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        Page<User> usersPage = stashClient.getUsers(null, 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        assertThat(captor.getValue().getUrl(),
                is("/rest/api/1.0/admin/users?start=0&limit=25"));

        assertThat(usersPage, page(User.class)
                .size(is(2))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build()
        );
        assertThat(usersPage.getValues().get(0), user()
                .name(is("jcitizen"))
                .emailAddress(is("jane@example.com"))
                .id(is(101L))
                .displayName(is("Jane Citizen"))
                .active(is(true))
                .slug(is("jcitizen"))
                .type(is("NORMAL"))
                .directoryName(is("Bitbucket Internal Directory"))
                .deletable(is(true))
                .lastAuthenticationTimestamp(is(new Date(1368145580548L)))
                .mutableDetails(is(true))
                .mutableGroups(is(true))
                .build()
        );
        assertThat(usersPage.getValues().get(1), user()
                .name(is("foobar"))
                .emailAddress(is("foobar@example.com"))
                .id(is(102L))
                .displayName(is("Foo Bar"))
                .active(is(true))
                .slug(is("foobar"))
                .type(is("NORMAL"))
                .directoryName(is("Bitbucket Internal Directory"))
                .deletable(is(true))
                .lastAuthenticationTimestamp(is(new Date(1368145580578L)))
                .mutableDetails(is(true))
                .mutableGroups(is(true))
                .build()
        );
    }

    private static Report getExampleReport() {
        return new Report(
                "bamboo-report",
                "Bamboo report",
                Collections.singletonList(new ReportDataEntry("Failed builds", 13L, ReportDataEntry.Type.NUMBER)),
                "Custom report generated by Atlassian Bamboo",
                "Atlassian Bamboo",
                "https://bamboo.atlassian.com",
                "https://bamboo.atlassian.com/logo.png",
                Report.Result.FAIL
        );
    }

    private Matcher<Report> getExampleReportMatcher() {
        final Report report = getExampleReport();
        return report()
                .key(is(report.getKey()))
                .title(is(report.getTitle()))
                .data(contains(report.getData().stream()
                        .map(entry -> reportDataEntry()
                                .title(is(entry.getTitle()))
                                .value(is(entry.getValue()))
                                .type(is(entry.getType()))
                                .build())
                        .collect(Collectors.toList())))
                .details(is(report.getDetails()))
                .vendor(is(report.getVendor()))
                .link(is(report.getLink()))
                .logoUrl(is(report.getLogoUrl()))
                .result(is(report.getResult()))
                .build();
    }

    @Test
    public void testGetReports() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPORTS);
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Page<Report> reportsPage = stashClient.getReports("PROJ", "repo", "9fa0f68733d", 0, 25);

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports?start=0&limit=25"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.GET));

        assertThat(reportsPage, page(Report.class)
                .size(is(1))
                .limit(is(25))
                .values(CoreMatchers.notNullValue())
                .build());

        assertThat(reportsPage.getValues().get(0), getExampleReportMatcher());
    }

    @Test
    public void testGetReportFound() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPORT);
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Optional<Report> reportOptional = stashClient.getReport("PROJ", "repo", "9fa0f68733d", "bamboo-report");

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.GET));

        assertThat(reportOptional.isPresent(), is(true));
        assertThat(reportOptional.get(), getExampleReportMatcher());
    }

    @Test
    public void testGetReportNotFound() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NOT_FOUND, TestData.REPORT_ERROR_NOT_FOUND);
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Optional<Report> reportOptional = stashClient.getReport("PROJ", "repo", "9fa0f68733d", "bamboo-report");

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.GET));

        assertThat(reportOptional.isPresent(), is(false));
    }

    @Test
    public void testCreateReport() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.REPORT);
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final Report createdReport = stashClient.createReport("PROJ", "repo", "9fa0f68733d", getExampleReport());

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.PUT));

        assertThat(createdReport, getExampleReportMatcher());
    }

    @Test
    public void testDeleteReport() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        stashClient.deleteReport("PROJ", "repo", "9fa0f68733d", "bamboo-report");

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.DELETE));
    }

    private static CodeAnnotation getExampleCodeAnnotation() {
        return new CodeAnnotation(
                "bamboo-report",
                "jira-main-npe",
                CodeAnnotation.Severity.MEDIUM,
                "May produce NullPointerException",
                "src/main/java/com/atlassian/jira/Main.java",
                75,
                "https://bamboo.atlassian.com/npe",
                CodeAnnotation.Type.CODE_SMELL
        );
    }

    private Matcher<CodeAnnotation> getExampleCodeAnnotationMatcher() {
        final CodeAnnotation annotation = getExampleCodeAnnotation();
        return codeAnnotation()
                .reportKey(is(annotation.getReportKey()))
                .severity(is(annotation.getSeverity()))
                .message(is(annotation.getMessage()))
                .path(is(annotation.getPath()))
                .externalId(is(annotation.getExternalId()))
                .line(is(annotation.getLine()))
                .link(is(annotation.getLink()))
                .type(is(annotation.getType()))
                .build();
    }

    @Test
    public void testGetCodeAnnotations() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_OK, TestData.CODE_ANNOTATIONS);
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        final List<CodeAnnotation> codeAnnotations = stashClient.getCodeAnnotations("PROJ", "repo", "9fa0f68733d");

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/annotations"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.GET));

        assertThat(codeAnnotations, hasSize(1));
        assertThat(codeAnnotations.get(0), getExampleCodeAnnotationMatcher());
    }

    @Test
    public void testCreateCodeAnnotations() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final CodeAnnotation annotation = getExampleCodeAnnotation();

        // when
        final boolean created = stashClient.createCodeAnnotations("PROJ", "repo", "9fa0f68733d", "bamboo-report",
                Collections.singleton(annotation));

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(created, is(true));
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.POST));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("annotations", hasSize(1))
                .assertThat("annotations[0].severity", is(annotation.getSeverity().name()))
                .assertThat("annotations[0].message", is(annotation.getMessage()))
                .assertThat("annotations[0].path", is(annotation.getPath()))
                .assertThat("annotations[0].externalId", is(annotation.getExternalId()))
                .assertThat("annotations[0].line", is(annotation.getLine()))
                .assertThat("annotations[0].link", is(annotation.getLink()))
                .assertThat("annotations[0].type", is(annotation.getType().name()));
    }

    @Test
    public void testCreateCodeAnnotationsLimitExceeded() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_ENTITY_TOO_LARGE, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final CodeAnnotation annotation = getExampleCodeAnnotation();

        // when
        final boolean created = stashClient.createCodeAnnotations("PROJ", "repo", "9fa0f68733d", "bamboo-report",
                Collections.singleton(annotation));

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(created, is(false));
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.POST));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("annotations", hasSize(1))
                .assertThat("annotations[0].severity", is(annotation.getSeverity().name()))
                .assertThat("annotations[0].message", is(annotation.getMessage()))
                .assertThat("annotations[0].path", is(annotation.getPath()))
                .assertThat("annotations[0].externalId", is(annotation.getExternalId()))
                .assertThat("annotations[0].line", is(annotation.getLine()))
                .assertThat("annotations[0].link", is(annotation.getLink()))
                .assertThat("annotations[0].type", is(annotation.getType().name()));
    }

    @Test
    public void testCreateCodeAnnotationsDuplicates() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_BAD_REQUEST, TestData.CODE_ANNOTATIONS_ERROR_DUPLICATE);

        // expect
        expectedException.expect(StashRestException.class);
        expectedException.expectMessage("Request contains duplicate externalIds");

        // when
        stashClient.createCodeAnnotations("PROJ", "repo", "9fa0f68733d", "bamboo-report",
                Arrays.asList(getExampleCodeAnnotation(), getExampleCodeAnnotation()));
    }

    @Test
    public void testUpdateCodeAnnotation() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final CodeAnnotation annotation = getExampleCodeAnnotation();

        // when
        final boolean createdOrUpdated = stashClient.updateCodeAnnotation("PROJ", "repo", "9fa0f68733d",
                "bamboo-report", "jira-main-npe", annotation);

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(createdOrUpdated, is(true));
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations/jira-main-npe"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.PUT));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.severity", is(annotation.getSeverity().name()))
                .assertThat("$.message", is(annotation.getMessage()))
                .assertThat("$.path", is(annotation.getPath()))
                .assertThat("$.externalId", is(annotation.getExternalId()))
                .assertThat("$.line", is(annotation.getLine()))
                .assertThat("$.link", is(annotation.getLink()))
                .assertThat("$.type", is(annotation.getType().name()));
    }

    @Test
    public void testUpdateCodeAnnotationLimitExceeded() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_ENTITY_TOO_LARGE, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);
        final CodeAnnotation annotation = getExampleCodeAnnotation();

        // when
        final boolean createdOrUpdated = stashClient.updateCodeAnnotation("PROJ", "repo", "9fa0f68733d",
                "bamboo-report", "jira-main-npe", annotation);

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(createdOrUpdated, is(false));
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations/jira-main-npe"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.PUT));

        JsonAssert.with(captor.getValue().getPayload())
                .assertThat("$.severity", is(annotation.getSeverity().name()))
                .assertThat("$.message", is(annotation.getMessage()))
                .assertThat("$.path", is(annotation.getPath()))
                .assertThat("$.externalId", is(annotation.getExternalId()))
                .assertThat("$.line", is(annotation.getLine()))
                .assertThat("$.link", is(annotation.getLink()))
                .assertThat("$.type", is(annotation.getType().name()));
    }

    @Test
    public void testDeleteCodeAnnotation() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        stashClient.deleteCodeAnnotation("PROJ", "repo", "9fa0f68733d", "bamboo-report", "jira-main-npe");

        // then
        verify(httpExecutor).execute(captor.capture(), any());
        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations" +
                        "?externalId=jira-main-npe"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.DELETE));
    }

    @Test
    public void testDeleteCodeAnnotations() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(HTTP_NO_CONTENT, "");
        final ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        stashClient.deleteCodeAnnotations("PROJ", "repo", "9fa0f68733d", "bamboo-report");

        // then
        verify(httpExecutor).execute(captor.capture(), any());

        assertThat(captor.getValue().getUrl(),
                is("/rest/insights/1.0/projects/PROJ/repos/repo/commits/9fa0f68733d/reports/bamboo-report/annotations"));
        assertThat(captor.getValue().getMethod(), is(HttpMethod.DELETE));
    }
}
